﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace trangwebbanhangasp.Models
{
	public class SanphamModel
	{
		public int id { get; set; }
		public Nullable<int> giongnhau { get; set; }
		public Nullable<int> id_loaihang { get; set; }
		public Nullable<int> id_phanloai { get; set; }
		public string ten_sp { get; set; }
		public Nullable<double> gia { get; set; }
		public Nullable<int> phantramgiamgia { get; set; }
		public Nullable<double> gia_moi { get; set; }
		public string size { get; set; }
		public string mo_ta_kieu_dang { get; set; }
		public string mo_ta_mau_sac { get; set; }
		public string hinh_anh { get; set; }
		public Nullable<int> soluonng { get; set; }
		public Nullable<int> id_noibat { get; set; }
	}
}