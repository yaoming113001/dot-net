﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace trangwebbanhangasp.Models
{
	public class Cart
	{
		public int id { get; set; }
		public string tensp { get; set; }
		public int soluong { get; set; }
		public double gia { get; set; }
		public string hinhanh { get; set; }
		public int phantram { get; set; }
		public int id_loaihang { get; set; }
		thoitrangnamEntities9 db = new thoitrangnamEntities9();
		public Cart(int idsach)
		{
			id = idsach;
			sanpham sp = db.sanphams.Single(n => n.id == id);
			tensp = sp.ten_sp;
			soluong = 1;
			gia = double.Parse(sp.gia.ToString());
			hinhanh = sp.hinh_anh;
			phantram = int.Parse(sp.phantramgiamgia.ToString());
			id_loaihang = int.Parse(sp.id_loaihang.ToString());
		}
	}
}