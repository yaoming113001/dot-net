﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace trangwebbanhangasp.Models
{
	public class DangkyModel
	{
		[Key]
		public int ID { set; get; }

		[Display(Name = "Tên đăng nhập")]
		[Required(ErrorMessage = "Yêu cầu nhập tên đăng nhập")]
		public string tenuser { get; set; }

		[Display(Name = "Mật khẩu")]
		[StringLength(20, MinimumLength = 6, ErrorMessage = "Độ dài mật khẩu ít nhất 6 ký tự.")]
		[Required(ErrorMessage = "Yêu cầu nhập mật khẩu")]
		public string pass { get; set; }


		[Display(Name = "Xác nhận mật khẩu")]
		[Compare("pass", ErrorMessage = "Xác nhận mật khẩu không đúng.")]
		public string repass { get; set; }

		[Display(Name = "Họ tên")]
		[Required(ErrorMessage = "Yêu cầu nhập họ tên")]
		public string ten_khach_hang { get; set; }

		[Display(Name = "Điện thoại")]
		public Nullable<int> sdt { get; set; }

		[Display(Name = "Giới tính")]
		public string gioi_tinh { get; set; }

		[Required(ErrorMessage = "Yêu cầu nhập email")]
		[Display(Name = "Email")]
		public string mail { get; set; }
	}
}