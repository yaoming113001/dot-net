﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace trangwebbanhangasp.Models
{
	public class Usermodel
	{
		thoitrangnamEntities9 thi = new thoitrangnamEntities9();
		public IEnumerable<user> laydl()
		{
			return thi.users.ToList();
		}
		public user lay(int id)
		{
			return thi.users.First(m => m.id_user.CompareTo(id) == 0);
		}
		public void sua(user hs)
		{
			user x = lay(hs.id_user);
			x.ten_khach_hang = hs.ten_khach_hang;
			x.mail = hs.mail;
			thi.SaveChanges();
		}
	}
}