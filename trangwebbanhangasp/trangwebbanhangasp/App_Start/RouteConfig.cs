﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace trangwebbanhangasp
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.IgnoreRoute("{*botdetect}",
		new { botdetect = @"(.*)BotDetectCaptcha\.ashx" });


			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Dangky",
				url: "dang-ky",
				defaults: new { controller = "Dangkydangnhap", action = "Dangky", id = UrlParameter.Optional },
				namespaces: new[] {"trangwebbanhangasp.Controllers"}
			);
			routes.MapRoute(
				name: "Themgiohang",
				url: "add",
				defaults: new { controller = "Giohang", action = "Themgiohang", id = UrlParameter.Optional },
				namespaces: new[] { "trangwebbanhangasp.Controllers" }
			); 
		}
	}
}
