﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using trangwebbanhangasp.Models;

namespace trangwebbanhangasp.Controllers
{
	public class CartController : Controller
	{
		thoitrangnamEntities9 db = new thoitrangnamEntities9();

		public List<Cart> Laygiohang()
		{
			List<Cart> listcart = Session["Giohang"] as List<Cart>;
			if (listcart == null)
			{
				listcart = new List<Cart>();
				Session["Giohang"] = listcart;
			}
			return listcart;
		}
		//them gio hang
		public ActionResult themgiohang(int idsp, string url)
		{
			sanpham sp = db.sanphams.SingleOrDefault(n => n.id == idsp);
			if (sp == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			//lay ra session gio hang
			List<Cart> list = Laygiohang();
			//kiem tra sp da ton tai trong session chua
			Cart cart = list.Find(n => n.id == idsp);
			if (cart == null)
			{
				cart = new Cart(idsp);
				list.Add(cart);
				return Redirect(url);
			}
			else
			{
				cart.soluong++;
				return Redirect(url);
			}
		}
		//bot gio hang
		public ActionResult botgiohang(int idsp, string url)
		{
			sanpham sp = db.sanphams.SingleOrDefault(n => n.id == idsp);
			if (sp == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			//lay ra session gio hang
			List<Cart> list = Laygiohang();
			//kiem tra sp da ton tai trong session chua
			Cart cart = list.Find(n => n.id == idsp);
			if (cart != null)
			{
				cart.soluong--;
				if (cart.soluong == 0)
				{
					list.Remove(cart);
					if (list.Count() == 0)
					{
						return RedirectToAction("CartEmt", "Cart");
					}
				}
				return Redirect(url);
			}
			else
			{
				return Redirect(url);
			}
		}
		//cap nhap gio hang
		public ActionResult capnhapgiohang(int idsp, FormCollection f)
		{
			//kiem tra ma sp
			sanpham sp = db.sanphams.SingleOrDefault(n => n.id == idsp);
			if (sp == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			List<Cart> list = Laygiohang();
			Cart cart = list.SingleOrDefault(n => n.id == idsp);
			if (cart != null)
			{
				cart.soluong = int.Parse(f["txtsoluong"].ToString());

			}
			return View("Cart");
		}
		//xoa gio hang
		public ActionResult xoagiohang(int idsp)
		{
			sanpham sp = db.sanphams.SingleOrDefault(n => n.id == idsp);
			if (sp == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			List<Cart> list = Laygiohang();
			Cart cart = list.SingleOrDefault(n => n.id == idsp);
			if (cart != null)
			{
				list.RemoveAll(n => n.id == idsp);

			}
			if (list.Count == 0)
			{
				return RedirectToAction("CartEmt", "Cart");
			}
			return RedirectToAction("Cart");
		}


		// GET: trang gio hang
		public ActionResult Cart()
		{

			List<Cart> list = Laygiohang();
			return View(list);
		}
		public ActionResult CartEmt()
		{

			return View();
		}
		private double tongtien()
		{
			double tong = 0;
			List<Cart> list = Session["Giohang"] as List<Cart>;
			if (list != null)
			{
				tong = list.Sum(n => (n.gia - (n.gia * n.phantram) / 100) * n.soluong);
			}
			return tong;
		}
		private string phiship()
		{
			double tong = 0;
			string phi = "";
			List<Cart> list = Session["Giohang"] as List<Cart>;
			if (list != null)
			{
				tong = list.Sum(n => (n.gia - (n.gia * n.phantram) / 100) * n.soluong);
				if (tong > 300000)
				{
					phi = "Miễn phí";
				}
				else
				{
					phi = "25,000 VND";
				}
			}
			return phi;
		}
		private double tongthanhtoan()
		{
			double tong = 0;
			List<Cart> list = Session["Giohang"] as List<Cart>;
			if (list != null)
			{
				tong = list.Sum(n => (n.gia - (n.gia * n.phantram) / 100) * n.soluong);
				if (tong > 300000)
				{
					tong = tong;
				}
				else
				{
					tong = tong + 25000;
				}
			}
			return tong;
		}
		private int tongsoluong()
		{
			int soluong = 0;
			List<Cart> list = Session["Giohang"] as List<Cart>;
			if (list != null)
			{
				soluong = list.Sum(n => n.soluong);
			}
			return soluong;
		}
		public ActionResult Cartpartial()
		{
			if (tongsoluong() == 0)
			{
				return PartialView();
			}
			ViewBag.tongsoluong = tongsoluong();
			ViewBag.tongtien = tongtien();
			return PartialView();
		}
		public ActionResult tong()
		{
			if (tongtien() == 0)
			{
				return PartialView();
			}
			ViewBag.tongsoluong = tongsoluong();
			ViewBag.tongtien = tongtien();
			return PartialView();
		}
		public ActionResult phitong()
		{

			ViewBag.phi = phiship();
			ViewBag.tongtien = tongtien();
			return PartialView();
		}
		public ActionResult phithanhtoan()
		{
			ViewBag.thanhtoan = tongthanhtoan();
			ViewBag.phi = phiship();
			ViewBag.tongtien = tongtien();
			return PartialView();
		}

		//xay dung view cho nguoi dung chinh sua gio hang
		public ActionResult SuaGioHang()
		{
			List<Cart> list = Laygiohang();
			return View(list);
		}
		//chức năng đặt hàng
		[HttpPost]
		public ActionResult Dathang()
		{
			//kiem tra dang nhập
			if (Session["Taikhoan"] == "" || Session["Taikhoan"] == null)
			{
				return RedirectToAction("Dangnhap", "Dangkydangnhap");
			}
			//don hang
			donhang dh = new donhang();
			List<Cart> gh = Laygiohang();
			user kh = (user)Session["Taikhoan"];
			dh.id_user = kh.id_user;
			dh.ngay_tao_hd = DateTime.Now;
			dh.diachi = kh.dia_chi1;
			dh.idthanhtoan = 2;
			dh.idtinhtrang = 2;
			dh.sdt = kh.sdt;
			dh.tong_tien = 0;
			db.donhangs.Add(dh);
			//them chi tiết đơn hàng
			foreach (var item in gh)
			{
				ctdh ctdh = new ctdh();
				ctdh.id_donhang = dh.id_donhang;
				ctdh.id_sanpham = item.id;
				ctdh.so_luong = item.soluong;
				ctdh.gia = (decimal)item.gia;
				ctdh.tong_tien = ctdh.so_luong * ctdh.gia;
				db.ctdhs.Add(ctdh);
				dh.tong_tien += ctdh.tong_tien;
				Session["Giohang"] = null;

			}
			db.SaveChanges();
			return RedirectToAction("dathangthanhcong", "Cart");
		}
		public ActionResult dathangthanhcong()
		{
			return View();
		}
		//sửa địa chỉ giao hàng
		[HttpGet]
		public ActionResult Edit()
		{
			var session = (user)Session["Taikhoan"];
			user user = db.users.SingleOrDefault(n => n.id_user == session.id_user);
			if (user == null)
			{
				Response.StatusCode = 404;
				return null;
			}

			return View(user);
		}
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(user user, FormCollection f)
		{

			user u = db.users.SingleOrDefault(n => n.id_user == user.id_user);
			u.dia_chi1 = user.dia_chi1;
			db.SaveChanges();
			return RedirectToAction("Cart");
		}
		//địa chỉ
		public PartialViewResult Details()
		{
			var session = (user)Session["Taikhoan"];
			user user = db.users.SingleOrDefault(n => n.id_user == session.id_user);
			if (user == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			return PartialView(user);
		}
	}
}