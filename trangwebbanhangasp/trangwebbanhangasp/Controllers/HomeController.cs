﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using trangwebbanhangasp.Dao;
using trangwebbanhangasp.Models;

namespace trangwebbanhangasp.Controllers
{
	public class HomeController : Controller
	{
		public PartialViewResult Taikhoan()
		{
			return PartialView();
		}

		//GET: Thoat
		public ActionResult Logout()
		{
			Session["Taikhoan"] = null;
			return RedirectToAction("Trangchu", "Trangchu");
		}

		// GET: Giohang
		public ActionResult Giohang()
		{
			return View();
		}
		// GET: Thongtin
		public ActionResult Thongtin()
		{
			return View();
		}
		// GET: Lienhe
		thoitrangnamEntities9 db = new thoitrangnamEntities9();
		[HttpGet]
		public ActionResult Lienhe()
		{
			return View();
		}
		[HttpPost]
		public ActionResult Lienhe(FormCollection f)
		{
			var dao = new UserDao();
			string tentk = f["txtTaiKhoan"].ToString();
			string mk = f.Get("txtMatKhau").ToString();
			phanhoi kh = new phanhoi();
			kh.email = tentk;
			kh.noidung = mk;
			kh.ngaygui = DateTime.Now;
			kh.idstatusgui = 1;
			var result = dao.Insertinphanhoi(kh);
			if (result > 0)
			{
				ViewBag.thanhcong = "Phản hồi đã được gửi, chúng tôi sẽ trả lời cho bạn sớm nhất có thể";
			}
			return View();
		}


		//Get timkiemauto
		public JsonResult GetSearchValue(string search)
		{
			List<SanphamModel> allsearch = db.sanphams.Where(x => x.ten_sp.Contains(search)).Select(x => new SanphamModel
			{
				ten_sp = x.ten_sp,

			}).Distinct().Take(10).ToList();
			return new JsonResult { Data = allsearch, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}
		public PartialViewResult Timkiem()
		{
			return PartialView();
		}

		//sua tai khoan

			}
}