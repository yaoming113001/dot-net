﻿using PagedList;
using PagedList.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using trangwebbanhangasp.Models;

namespace trangwebbanhangasp.Controllers
{
    public class TimkiemController : Controller
    {
		thoitrangnamEntities9 db = new thoitrangnamEntities9();
		[HttpPost]
		public ActionResult Trangtimkiem(FormCollection f, int? page)
		{
			string tukhoa = f["txtTimkiem"].ToString();
			List<sanpham> list = db.sanphams.Where(n => n.ten_sp.Contains(tukhoa)).ToList();
			int pagenumber = (page ?? 1);
			int pagesize = 12;
			if (list.Count == 0)
			{
				ViewBag.ThongBao = "Không tìm thấy sản phẩm nào";
				return View(db.sanphams.OrderBy(n => n.ten_sp).ToPagedList(pagenumber, pagesize));
			}

			ViewBag.ThongBao = "Đã tìm thấy " + list.Count + " kết quả";
			return View(list.OrderBy(n => n.ten_sp).ToPagedList(pagenumber, pagesize));
		}
		public JsonResult ListName(string tukhoa)
		{
			var data = List(tukhoa);
			return Json(new { data, status = true }, JsonRequestBehavior.AllowGet);
		}
		public List<string> List(string key)
		{
			return db.sanphams.Where(n => n.ten_sp.Contains(key)).Select(n => n.ten_sp).ToList();
		}
	}
}