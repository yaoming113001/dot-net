﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using trangwebbanhangasp.Models;
using trangwebbanhangasp.Dao;
using BotDetect.Web.Mvc;
using Facebook;
using System.Configuration;

namespace trangwebbanhangasp.Controllers
{
	public class DangkydangnhapController : Controller
	{
		thoitrangnamEntities9 db = new thoitrangnamEntities9();
		//GET: dangnhap
		[HttpGet]
		public ActionResult Dangnhap()
		{
			return View();
		}
		[HttpPost]
		public ActionResult Dangnhap(FormCollection f)
		{
			string tentk = f["txtTaiKhoan"].ToString();
			string mk = f.Get("txtMatKhau").ToString();
			var dao = new UserDao();
			string pass = dao.ToMD5(mk);
			user kh = db.users.SingleOrDefault(n => n.tenuser == tentk && n.pass == pass && n.nhom == "2");
			if (kh != null)
			{
				ViewBag.ThongBao = "Chúc mừng bạn đăng nhập thành công";

				Session["Taikhoan"] = kh;
				return new RedirectResult("/Trangchu/Trangchu");
			}
			ViewBag.ThongBao = "Tên tài khoản hoặc mật khẩu không đúng";
			return View();
		}

		[HttpGet]
		public ActionResult Dangky()
		{
			return View();
		}
		[HttpPost]
		public ActionResult Dangky(DangkyModel model)
		{
			if (ModelState.IsValid)
			{
				var dao = new UserDao();
				if (dao.CheckUserName(model.tenuser))
				{
					ModelState.AddModelError("", "Tên đăng nhập đã tồn tại");
				}
				else if (dao.CheckEmail(model.mail))
				{
					ModelState.AddModelError("", "Email đã tồn tại");
				}
				else if (dao.CheckPassName(model.mail))
				{
					ModelState.AddModelError("", "Mật khẩu đã tồn tại");
				}
				else if (dao.isEmail(model.mail) == false)
				{
					ModelState.AddModelError("", "Email không hợp lệ");
				}
				else if (dao.gioitinh(model.gioi_tinh) == false)
				{
					ModelState.AddModelError("", "Giới tính không hợp lệ");
				}
				else
				{
					var user = new user();
					user.tenuser = model.tenuser;
					user.pass = dao.ToMD5(model.pass);
					user.mail = model.mail;
					user.gioi_tinh = model.gioi_tinh;
					user.sdt = model.sdt;
					user.ten_khach_hang = model.ten_khach_hang;
					user.nhom = "2";

					var result = dao.Insert(user);
					if (result > 0)
					{
						ViewBag.Success = "Đăng ký thành công";
						model = new DangkyModel();
						return new RedirectResult("/Dangkydangnhap/dangnhapthanhcong");
					}
					else
					{
						ModelState.AddModelError("", "Đăng ký không thành công.");
					}

				}
			}
			return View(model);
		}
		public ActionResult dangnhapthanhcong()
		{
			return View();
		}

		//dang nhap bang facebook

		private Uri RedirectUri
		{
			get
			{
				var uriBuilder = new UriBuilder(Request.Url);
				uriBuilder.Query = null;
				uriBuilder.Fragment = null;
				uriBuilder.Path = Url.Action("FacebookCallback");
				return uriBuilder.Uri;
			}
		}
		
		public ActionResult LoginFacebook()
		{
			var fb = new FacebookClient();
			var loginUrl = fb.GetLoginUrl(new
			{
				client_id = ConfigurationManager.AppSettings["FbAppId"],
				client_secret = ConfigurationManager.AppSettings["FbAppSecret"],
				redirect_uri = RedirectUri.AbsoluteUri,
				response_type = "code",
				scope = "email",
			});
			return Redirect(loginUrl.AbsoluteUri);
		}
		public ActionResult FacebookCallback(string code)
		{
			var fb = new FacebookClient();
			dynamic result = fb.Post("oauth/access_token", new
			{
				client_id = ConfigurationManager.AppSettings["FbAppId"],
				client_secret = ConfigurationManager.AppSettings["FbAppSecret"],
				redirect_uri = RedirectUri.AbsoluteUri,
				code = code
			});
			  

			var accessToken = result.access_token;
			if (!string.IsNullOrEmpty(accessToken))
			{
				fb.AccessToken = accessToken;
				// Get the user's information, like email, first name, middle name etc
				dynamic me = fb.Get("me?fields=first_name,middle_name,last_name,id,email");
				string email = me.email;
				string userName = me.email;
				string firstname = me.first_name;
				string middlename = me.middle_name;
				string lastname = me.last_name;

				var user = new user();
				user.mail = email;
				user.tenuser = userName;	
				user.ten_khach_hang = firstname + " " + middlename + " " + lastname;
				user.nhom = "2";
				
				var resultInsert = new UserDao().InsertForFacebook(user);
				if (resultInsert > 0)
				{
					ViewBag.ThongBao = "Chúc mừng bạn đăng nhập thành công";

					Session["Taikhoan"] = user;
					return new RedirectResult("/Trangchu/Trangchu");
				}
			}
			return Redirect("/");
		}
	}
}