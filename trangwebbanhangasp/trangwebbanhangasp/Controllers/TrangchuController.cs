﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using trangwebbanhangasp.Models;

namespace trangwebbanhangasp.Controllers
{
    public class TrangchuController : Controller
    {
		thoitrangnamEntities9 dbo = new thoitrangnamEntities9();
		// GET: Trangchu
		public ActionResult Trangchu()
        {
            return View();
        }
		public PartialViewResult Danhmucao()
		{
			return PartialView(dbo.phanloais.Where(n => n.cungphanloai == 1).Take(4).ToList());
		}
		public PartialViewResult Danhmucquan()
		{
			return PartialView(dbo.phanloais.Where(n => n.cungphanloai == 2).Take(4).ToList());
		}
		public PartialViewResult Danhmucgiay()
		{
			return PartialView(dbo.phanloais.Where(n => n.cungphanloai == 3).Take(4).ToList());
		}
		public PartialViewResult Aohot()
		{
			return PartialView(dbo.sanphams.Where(n => n.id_loaihang == 1 && n.id_noibat==1).Take(4).ToList());
		}
		public PartialViewResult Quanhot()
		{
			return PartialView(dbo.sanphams.Where(n => n.id_loaihang == 2 && n.id_noibat==1).Take(4).ToList());
		}
		public PartialViewResult Giayhot()
		{
			return PartialView(dbo.sanphams.Where(n => n.id_loaihang == 3 && n.id_noibat == 1).Take(4).ToList());
		}
		public ViewResult Chitietsanpham(int IDsanpham)
		{
			sanpham sp = dbo.sanphams.SingleOrDefault(n => n.id == IDsanpham);
			if (sp == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			return View(sp);
		}
		
		public ViewResult SanphamtheodanhmucAo(int IDphanloai)
		{
			phanloai pl = dbo.phanloais.SingleOrDefault(n => n.id_phanloai == IDphanloai);
			{
				Response.StatusCode = 404;
			}
			List<sanpham> list = dbo.sanphams.Where(n => n.id_phanloai == IDphanloai).OrderBy(n => n.gia).ToList();
			if (list.Count == 0)
			{
				ViewBag.sanpham = "Không có sản phẩm nào thuộc chủ đề cần tìm";
			}
			return View(list);
		}
		public ViewResult SanphamtheodanhmucQuan(int IDphanloai)
		{
			phanloai pl = dbo.phanloais.SingleOrDefault(n => n.id_phanloai == IDphanloai);
			{
				Response.StatusCode = 404;
			}
			List<sanpham> list = dbo.sanphams.Where(n => n.id_phanloai == IDphanloai).OrderBy(n => n.gia).ToList();
			if (list.Count == 0)
			{
				ViewBag.sanpham = "Không có sản phẩm nào thuộc chủ đề cần tìm";
			}
			return View(list);
		}
		public ViewResult SanphamtheodanhmucGiay(int IDphanloai)
		{
			phanloai pl = dbo.phanloais.SingleOrDefault(n => n.id_phanloai == IDphanloai);
			{
				Response.StatusCode = 404;
			}
			List<sanpham> list = dbo.sanphams.Where(n => n.id_phanloai == IDphanloai).OrderBy(n => n.gia).ToList();
			if (list.Count == 0)
			{
				ViewBag.sanpham = "Không có sản phẩm nào thuộc chủ đề cần tìm";
			}
			return View(list);
		}
		//goi y san pham hot
		public PartialViewResult GoiyAo(int idsanpham)
		{
			sanpham pl = dbo.sanphams.SingleOrDefault(n => n.id == idsanpham);
			{
				Response.StatusCode = 404;
			}
			List<sanpham> list = dbo.sanphams.Where(n => n.id_noibat == pl.id_noibat & n.id_loaihang == pl.id_loaihang).Take(6).Distinct().ToList();

			return PartialView(list);
		}
		//goi y san pham
		public PartialViewResult Sanphamhot(int idsanpham)
		{
			sanpham pl = dbo.sanphams.SingleOrDefault(n => n.id == idsanpham);
			{
				Response.StatusCode = 404;
			}
			List<sanpham> list = dbo.sanphams.Where(n => n.giongnhau == pl.giongnhau).Take(5).OrderBy(n => n.gia).ToList();
			
			return PartialView(list);
		}
		//danh mục áo đầy đủ
		public PartialViewResult Danhmucaodd()
		{
			return PartialView(dbo.phanloais.Where(n => n.cungphanloai == 1).ToList());
		}
		public PartialViewResult Danhmucquandd()
		{
			return PartialView(dbo.phanloais.Where(n => n.cungphanloai == 2).ToList());
		}
		public PartialViewResult Danhmucgiaydd()
		{
			return PartialView(dbo.phanloais.Where(n => n.cungphanloai == 3).ToList());
		}
	}
}