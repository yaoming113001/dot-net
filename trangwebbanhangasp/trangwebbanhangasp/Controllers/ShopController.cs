﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using trangwebbanhangasp.Models;

namespace trangwebbanhangasp.Controllers
{
    public class ShopController : Controller
    {
		thoitrangnamEntities9 dbo = new thoitrangnamEntities9();
		// GET: Danhsachsanpham
		public ActionResult Danhsachsanpham()
		{
			return View();
		}
		public PartialViewResult Aohot(int? page)
		{
			int pagesize = 8; //so san pham 1 trang
			int pagenumber = (page ?? 1); //so trang
			var listspHost = dbo.sanphams.Where(n => n.phantramgiamgia==10 && n.id_loaihang == 1).ToList().OrderBy(n => n.gia).ToPagedList(pagenumber, pagesize);
			return PartialView(listspHost);
		}
		public PartialViewResult Quanhot(int? page)
		{
			int pagesize = 8; //so san pham 1 trang
			int pagenumber = (page ?? 1); //so trang
			var listspHost = dbo.sanphams.Where(n => n.phantramgiamgia == 10 && n.id_loaihang == 2).ToList().OrderBy(n => n.gia).ToPagedList(pagenumber, pagesize);
			return PartialView(listspHost);
		}
		public PartialViewResult Giayhot(int? page)
		{
			int pagesize = 8; //so san pham 1 trang
			int pagenumber = (page ?? 1); //so trang
			var listspHost = dbo.sanphams.Where(n => n.phantramgiamgia == 10 && n.id_loaihang == 3).ToList().OrderBy(n => n.gia).ToPagedList(pagenumber, pagesize);
			return PartialView(listspHost);
		}

		public PartialViewResult Sanphamcungloai()
		{
			List<sanpham> list = dbo.sanphams.Where(n => n.id_noibat == 1 & n.id_loaihang == 1).Take(6).ToList();

			return PartialView(list);
		}
		public ActionResult Danhsachao()
		{
			return View();
		}
		public PartialViewResult Ao(int? page)
		{
			int pagesize = 12; //so san pham 1 trang
			int pagenumber = (page ?? 1); //so trang
			var listspHost = dbo.sanphams.Where(n => n.id_loaihang == 1).ToList().OrderBy(n => n.gia).ToPagedList(pagenumber, pagesize);
			return PartialView(listspHost);
		}
		public ActionResult Danhsachquan()
		{
			return View();
		}
		public PartialViewResult Quan(int? page)
		{
			int pagesize = 12; //so san pham 1 trang
			int pagenumber = (page ?? 1); //so trang
			var listspHost = dbo.sanphams.Where(n => n.id_loaihang == 2).ToList().OrderBy(n => n.gia).ToPagedList(pagenumber, pagesize);
			return PartialView(listspHost);
		}
		public ActionResult Danhsachgiay()
		{
			return View();
		}
		public PartialViewResult Giay(int? page)
		{
			int pagesize = 12; //so san pham 1 trang
			int pagenumber = (page ?? 1); //so trang
			var listspHost = dbo.sanphams.Where(n => n.id_loaihang == 3).ToList().OrderBy(n => n.gia).ToPagedList(pagenumber, pagesize);
			return PartialView(listspHost);
		}
		
	}
}