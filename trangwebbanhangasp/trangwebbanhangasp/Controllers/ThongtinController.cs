﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using trangwebbanhangasp.Dao;
using trangwebbanhangasp.Models;

namespace trangwebbanhangasp.Controllers
{
	public class ThongtinController : Controller
	{
		// GET: Thongtin
		Usermodel thi = new Usermodel();
		thoitrangnamEntities9 db = new thoitrangnamEntities9();
		//Chi tiet user
		public ActionResult Details()
		{
			var session = (user)Session["Taikhoan"];
			user user = db.users.SingleOrDefault(n => n.id_user == session.id_user);
			if (user == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			return View(user);
		}
		//Sua user
		[HttpGet]
		public ActionResult Edit()
		{
			var session = (user)Session["Taikhoan"];
			user user = db.users.SingleOrDefault(n => n.id_user == session.id_user);
			if (user == null)
			{
				Response.StatusCode = 404;
				return null;
			}

			return View(user);
		}
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(user user, FormCollection f)
		{
			var dao = new UserDao();

			user u = db.users.SingleOrDefault(n => n.id_user == user.id_user);
			u.ten_khach_hang = user.ten_khach_hang;
			if (u.pass.Equals(user.pass))
			{
				u.pass = user.pass;
			}
			else
			{
				u.pass = user.pass;
				u.pass = dao.ToMD5(user.pass);

			}
			u.tenuser = user.tenuser;
			u.ngay_sinh = user.ngay_sinh;
			u.mail = user.mail;
			u.sdt = user.sdt;
			u.gioi_tinh = user.gioi_tinh;
			u.dia_chi1 = user.dia_chi1;

			db.SaveChanges();

			return RedirectToAction("Details");
		}
	}

}