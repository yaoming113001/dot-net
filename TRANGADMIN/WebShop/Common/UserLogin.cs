﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebShop.Common
{
    [Serializable]
    public class UserLogin
    {
        public long id { set; get; }
        public string username { set; get; }
    }
}