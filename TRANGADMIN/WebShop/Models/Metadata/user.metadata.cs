﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using 2 thu vien thiet ke metadata
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebShop.Models
{
    [MetadataTypeAttribute(typeof(userMetadata))]
    public partial class user
    {
        // chi su dung cho 1 class va k cho ke thua
        internal sealed class userMetadata
        {
            [Display(Name = "User ID")]
            public int id_user { get; set; }
            [Display(Name = "User NameAccount")]
            public string tenuser { get; set; }
            [Display(Name = "User Password")]
            public string pass { get; set; }
            [Display(Name = "User Group")]
            public string nhom { get; set; }
            [Display(Name = "User Name")]
            public string ten_khach_hang { get; set; }
            [Display(Name = "User Phone")]
            public Nullable<int> sdt { get; set; }
            [Display(Name = "User Gender")]
            public string gioi_tinh { get; set; }
            [Display(Name = "User Adress")]
            public string dia_chi1 { get; set; }
            [Display(Name = "User Email")]
            public string mail { get; set; }
            
            [DataType(DataType.Date)]
            [DisplayFormat(DataFormatString = "{0:dd//MM//yyyy}", ApplyFormatInEditMode = true)]
            [Display(Name = "User Date")]
            public Nullable<System.DateTime> ngay_sinh { get; set; }
        }
    }
}