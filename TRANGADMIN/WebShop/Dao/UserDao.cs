﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using WebShop.Models;

namespace WebShop.Dao
{
    public class UserDao
    {
        thoitrangnamEntities4 db = null;
        public UserDao()
        {
            db = new thoitrangnamEntities4();
        }
        public user getByName (string username)
        {
            return db.users.SingleOrDefault(n => n.tenuser == username);
        }

        public bool Login(string tenuser, string pass)
        {
            var result = db.users.Count(n => n.tenuser == tenuser && n.pass == pass && n.nhom == "1");
            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
		//cua DUC
		public long Insert(user entity)
		{
			db.users.Add(entity);
			db.SaveChanges();
			return entity.id_user;
		}
		public long Insertinphanhoi(phanhoi entity)
		{
			db.phanhois.Add(entity);
			db.SaveChanges();
			return entity.idphanhoi;
		}
		public bool CheckUserName(string userName)
		{
			return db.users.Count(x => x.tenuser == userName) > 0;
		}
		public bool CheckPassName(string userName)
		{
			return db.users.Count(x => x.pass == userName) > 0;
		}
		public bool CheckEmail(string email)
		{

			return db.users.Count(x => x.mail == email) > 0;

		}
		public bool isEmail(string inputEmail)
		{
			inputEmail = inputEmail ?? string.Empty;
			string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
				  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
				  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
			Regex re = new Regex(strRegex);
			if (re.IsMatch(inputEmail))
				return (true);
			else
				return (false);
		}
		public bool gioitinh(string gt)
		{
			if (gt.Equals("Nam") || gt.Equals("nam") || gt.Equals("Nữ") || gt.Equals("Nu") || gt.Equals("nữ") || gt.Equals("nu"))
			{
				return (true);
			}
			else
			{
				return (false);
			}

		}

		public string ToMD5(string str)
		{
			string result = "";
			byte[] buffer = Encoding.UTF8.GetBytes(str);
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
			buffer = md5.ComputeHash(buffer);
			for (int i = 0; i < buffer.Length; i++)
			{
				result += buffer[i].ToString("x2");
			}
			return result;
		}


	}
}