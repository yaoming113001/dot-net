﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.Common;
using WebShop.Models;

namespace WebShop.Areas.Admin.Controllers
{
    public class QuanLyUserController : Controller
    {
        thoitrangnamEntities4 db = new thoitrangnamEntities4();
        // GET: Admin/QuanLyUser
        public ActionResult Index(string searchString, int? page)
        {
            thoitrangnamEntities4 db = new thoitrangnamEntities4();
            //Phan trang
            //int pageSize = 5;
            //int pageNumber = (page ?? 1);
            // if (searchString == null)
            // {
            //    ViewBag.ThongBao = "No User Searching";
            //    return View(db.users.ToList().OrderBy(n => n.id_user).ToPagedList(pageNumber, pageSize));
            //}
            List<user> lstKq = db.users.Where(n=>n.nhom=="2").ToList();
            return View(db.users.Where(n => n.tenuser.Contains(searchString) || searchString == null).ToList().ToPagedList(page?? 1,5));
            //Phan trang
            //int pageSize = 5;
            //int pageNumber = (page ?? 1);
           // return View(db.users.ToList().OrderBy(n => n.id_user).ToPagedList(pageNumber, pageSize));
        }


        //THEM MOI
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(user user)
        {
            user.pass.ToMD5();
            db.users.Add(user);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //Chi tiet user
        public ActionResult Details(int? id)
        {
            user user = db.users.SingleOrDefault(n => n.id_user == id);
            if (user == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(user);
        }

        //xóa user
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            user user = db.users.SingleOrDefault(n => n.id_user == id);
            if (user == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult Delete(int id)
        {
            user user = db.users.SingleOrDefault(n => n.id_user == id);
            if (user == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //Sua user
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            user user = db.users.SingleOrDefault(n => n.id_user == id);
            if (user == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            return View(user);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(user user, FormCollection f)
        {
            if (ModelState.IsValid)
            {
                //Thuc hien cap nhat trong model
                db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}