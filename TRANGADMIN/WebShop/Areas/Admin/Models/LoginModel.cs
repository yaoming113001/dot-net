﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebShop.Areas.Admin.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Pls, Enter your User Name")]
        public string username { set; get; }

        [Required(ErrorMessage = "Pls, Enter your Password")]
        public string password { set; get; }

        public bool rememberme { set; get; }

        public string nhom { set; get; }
    }
}